Plugins = {
    {'wbthomason/packer.nvim'},

    -- Ui
    {'vim-airline/vim-airline',            requires = {'vim-airline/vim-airline-themes'}},
    {'joshdick/onedark.vim'},
    {'drewtempelmeyer/palenight.vim'},
    {'sainnhe/gruvbox-material'},
    {'sheerun/vim-polyglot'},
    {'terrortylor/nvim-comment'},
    {'lewis6991/gitsigns.nvim',            requires = {'nvim-lua/plenary.nvim'}},
    {'lukas-reineke/indent-blankline.nvim'},
    -- {'kyazdani42/nvim-tree.lua'},
    -- {'norcalli/nvim-colorizer.lua'},       -- render bug (fixable with buffer reload)
    -- TODO: Add nvimTree and nvim-toggleterm plugins

    -- Completion
    {'neovim/nvim-lspconfig'},
    {'kabouzeid/nvim-lspinstall'},
    {'hrsh7th/nvim-compe'},
    {'ray-x/lsp_signature.nvim'},

    -- Tools
    {'sbdchd/neoformat'},
    {'farmergreg/vim-lastplace'},
    {'iamcco/markdown-preview.nvim',       config = "vim.call('mkdp#util#install')"},
    {'nvim-telescope/telescope.nvim',      requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}},
    {'tpope/vim-fugitive'},
    {'akinsho/nvim-toggleterm.lua'},
    {'zsugabubus/crazy8.nvim'},
}

Keymaps = {
    'navegation',
    -- 'nvim_bufferline',
    'nvim_comment',
    'indent_blankline',
    'nvim_toggleterm',
    'packer',
    'markdown_preview',
    'misc',
    'telescope',
    'shifting',
    -- 'autocomplete',
    'compe',
    'lsp',
}

Configs = {
    'nvim_comment',
    -- 'nvim_tree',
    'airline',
    'gitsigns',
    'indent_blankline',
    -- 'colorizer',
    'compe',
    'lsp_signature',
    -- 'nvim_bufferline',
}

Theme_curr = 3
Themes = {
    'onedark',
    'palenight',
    'gruvbox',
}

Miscs = {
    'format_on_save',
    'altered_comments',
}

local K = {}

function K.navegation()
    -- alt to move between buffers
    map(N, '<A-h>',             '<C-w>h')
    map(N, '<A-j>',             '<C-w>j')
    map(N, '<A-k>',             '<C-w>k')
    map(N, '<A-l>',             '<C-w>l')
    -- tabs actions
    map(N, '<C-t>',             ':tabnew<CR>')
    map(N, '<A-n>',             ':bnext<CR>')
    map(N, '<A-p>',             ':bprevious<CR>')
    -- tab switching with alt + {1-9}
    map(N, '<A-1>', ':bfirst<CR>')
    map(N, '<A-2>', ':bfirst<CR>:bn<CR>')
    map(N, '<A-3>', ':bfirst<CR>:2bn<CR>')
    map(N, '<A-4>', ':bfirst<CR>:3bn<CR>')
    map(N, '<A-5>', ':bfirst<CR>:4bn<CR>')
    map(N, '<A-6>', ':bfirst<CR>:5bn<CR>')
    map(N, '<A-7>', ':bfirst<CR>:6bn<CR>')
    map(N, '<A-8>', ':bfirst<CR>:7bn<CR>')
    map(N, '<A-9>', ':bfirst<CR>:8bn<CR>')
end

-- function K.nvim_bufferline()
--     map(N, '<A-n>',             ':BufferLineCycleNext<CR>')
--     map(N, '<A-p>',             ':BufferLineCyclePrev<CR>')
--     -- buffer switching with alt + {1-9}
--     for n = 1, 9 do
--         map(N, '<A-'..n..'>', ":lua require'bufferline'.go_to_buffer("..n..')<CR>')
--     end
-- end

function K.nvim_comment()
    map(N, '<leader>/',         ':CommentToggle<CR>')
    map(V, '<leader>/',         ':CommentToggle<CR>')
end

function K.indent_blankline()
    map(N, '<leader>i',         ':IndentBlanklineToggle<CR>')
end

function K.nvim_toggleterm()
    -- map(N, '???', ':ToggleTerm<CR>') -- TODO: add a good keymap
end

function K.packer()
    map(N, '<leader>p',         ':PackerSync<CR>')
end

function K.markdown_preview()
    map(N, '<leader>m',         ':MarkdownPreview<CR>')
end

function K.misc()
    -- toggle highligh search
    map(N, '<leader>h',         ':set invhlsearch<CR>')
    -- toggle spellcheck
    map(N, '<leader>s',         ':set invspell<CR>')
    -- cycle between installed themes
    map(N, '<leader>t',         ':ThemeCycle<CR>')
    -- making Y act like C and D
    map(N, 'Y',                 'y$')
end

function K.telescope()
    map(N, '<leader>tc',        ':Telescope command_history<CR>')
    map(N, '<leader>f',        ':Telescope find_files<CR>')
end

function K.shifting()
    map(N, 'J',             ':m .+1<CR>==')
    map(N, 'K',             ':m .-2<CR>==')
    map(V, 'J',             ":m '>+1<CR>gv=gv")
    map(V, 'K',             ":m '<-2<CR>gv=gv")
    -- making > and < indent lines
    map(N,  '>',            '>>_')
    map(N,  '<',            '<<_')
    map(V,  '>',            '>gv')
    map(V,  '<',            '<gv')
end

function K.compe()
    map(I,  '<CR>',             "compe#confirm('<CR>')", { noremap = true, silent = true, expr = true })
end

function K.lsp()
    -- TODO: remap some lsp keymaps
    map(N, 'gD',                '<Cmd>lua vim.lsp.buf.declaration()<CR>')
    map(N, 'gd',                '<Cmd>lua vim.lsp.buf.definition()<CR>')
    -- map(N, 'K',                 '<Cmd>lua vim.lsp.buf.hover()<CR>')
    map(N, 'gi',                '<cmd>lua vim.lsp.buf.implementation()<CR>')
    map(N, '<C-k>',             '<cmd>lua vim.lsp.buf.signature_help()<CR>')
    map(N, '<leader>wa',        '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>')
    map(N, '<leader>wr',        '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>')
    map(N, '<leader>wl',        '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')
    map(N, '<leader>D',         '<cmd>lua vim.lsp.buf.type_definition()<CR>')
    map(N, '<leader>rn',        '<cmd>lua vim.lsp.buf.rename()<CR>')
    map(N, '<leader>ka',        '<cmd>lua vim.lsp.buf.code_action()<CR>')
    map(N, 'gr',                '<cmd>lua vim.lsp.buf.references()<CR>')
    map(N, '<leader>e',         '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>')
    map(N, '[d',                '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
    map(N, ']d',                '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
    map(N, '<leader>q',         '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>')
    -- map(N, '<leader>f',         '<cmd>lua vim.lsp.buf.formatting()<CR>')
end

for _, keymap in pairs(Keymaps) do K[keymap]() end

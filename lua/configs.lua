local C = {}

function C.nvim_comment()
    require 'nvim_comment'.setup {
        create_mappings = false,
        marker_padding = true,
    }
end

function C.gitsigns()
    require 'gitsigns'.setup {
        signs = {
            add          = {hl = 'GitSignsAdd'   , text = '▎', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
            change       = {hl = 'GitSignsChange', text = '▎', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
            topdelete    = {hl = 'GitSignsDelete', text = '▎', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
            delete       = {hl = 'GitSignsDelete', text = '▎', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
            changedelete = {hl = 'GitSignsChange', text = '▎', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
        }
    }
end

function C.indent_blankline()
    -- let('indent_blankline_char', '│')
    -- let('indent_blankline_char', '')
    -- let('indent_blankline_char', '┆')
    let('indent_blankline_char', '¦')
    let('indent_blankline_filetype_exclude', {
        'help',
        'defx',
        'vimwiki',
        'man',
        'gitmessengerpopup',
        'diagnosticpopup',
        'packer',
    })
end

function C.nvim_tree()
    let('nvim_tree_disable_netrw', '0')
    let('nvim_tree_indent_markers', '1')
    let('nvim_tree_auto_close', '1')
    let('nvim_tree_lsp_diagnostics', '1')
end

function C.airline()
    let('airline#extensions#tabline#enabled', '1')
    let('airline#extensions#tabline#buffer_idx_mode', '1')

    let('airline_left_sep', '')
    let ('airline_left_alt_sep','')
    let('airline_right_sep', '')
    let ('airline_left_alt_sep', '')

    let('airline#extensions#tabline#left_sep', ' ')
    let('airline#extensions#tabline#left_alt_sep', '')
    let('airline#extensions#tabline#right_sep', ' ')
    let('airline#extensions#tabline#right_alt_sep', '')
end

function C.compe()
    require 'compe'.setup {
        enabled = true,
        debug = false,
        autocomplete = true,
        documentation = true,
        preselect = 'enable',
        min_length = 1,
        throttle_time = 80,
        source_timeout = 200,
        incomplete_delay = 400,
        max_abbr_width = 100,
        max_kind_width = 100,
        max_menu_width = 100,

        source = {
            buffer = true,
            tags = true,
            spell = true,
            path = true,
            calc = true,
            nvim_lsp = true,

            nvim_lua = false,
            vsnip = false,

            -- buffer = {kind = "  (Buff)",priority = 1},
            -- tags = true,
            -- spell = true,
            -- path = {kind = "  (Path)"},
            -- calc = true;
            -- nvim_lsp = {kind = "  (Lsp)",priority = 2},
            -- nvim_lua = true;
            -- vsnip = {kind = "  (Snip)",priority = 1},
        }
    }
end

function C.lsp_signature()
    require'lsp_signature'.on_attach({
        bind = true, -- This is mandatory, otherwise border config won't get registered.
        handler_opts = {
        border = 'none',
        }
    })
end

for _, config in pairs(Configs) do C[config]() end

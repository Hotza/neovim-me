T = {}

function T.onedark()
    let('onedark_termcolors', '16')
    cmd 'colo onedark'
    let('AirlineTheme', 'onedark')
end

function T.palenight()
    cmd 'colo palenight'
    let('AirlineTheme', 'palenight')
end

function T.gruvbox()
    let('gruvbox_material_background', 'soft')
    -- let('gruvbox_material_disable_italic_comment', '1')
    let('gruvbox_material_enable_italic', '1')
    cmd 'colo gruvbox-material'
    let('AirlineTheme', 'gruvbox-material')
end

local function ThemeChangeBefore()
end

local function ThemeChangeAfter()
end

function ThemeSet(n)
    n = tonumber(n)
    local theme = Themes[n]
    if (type(theme) == 'string') then
        ThemeChangeBefore()
        T[theme]()
        ThemeChangeAfter()
    end

    if (type(theme) == 'table') then
        ThemeChangeBefore()
        T[theme[1]](theme[2])
        ThemeChangeAfter()
    end
end
command('ThemeSet lua ThemeSet(<f-args>)', 1)

-- Cycle between intalled themes TODO: WIP
function ThemeCycle()
    Theme_curr = Theme_curr + 1
    if (Theme_curr > #Themes) then Theme_curr = 1 end
    if (Theme_curr < 1) then Theme_curr = #Themes end
    ThemeSet(Theme_curr)
end
command 'ThemeCycle lua ThemeCycle()'

ThemeSet(Theme_curr)
